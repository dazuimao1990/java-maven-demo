-- liquibase formatted sql
-- changeset guox.goodrain:1
create table another_person (
id int primary key,
name varchar(50) not null,
address1 varchar(50),
address2 varchar(50),
city varchar(30)
);
-- rollback drop table another_person;
-- changeset guox.goodrain:2
create table another_company (
id int primary key,
name varchar(50) not null,
address1 varchar(50),
address2 varchar(50),
city varchar(30)
);
-- rollback drop table another_company;